package ch.fhnw.masam;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@Service
public class CovidService {
	
	private static final Logger LOG = LogManager.getLogger(CovidService.class);

	@Autowired
    private CovidRepository covidRepository;
    
    public List<CovidCase> findByDate(String date) {
    	LOG.debug("Debug Message in find by date");
    	LOG.fatal("Fatal Message in find by date");
    	
    	List<CovidCase> result = covidRepository.findByDate(date);
    	LOG.info("query by date " + date + ": " + result.size());     	
    	return result;
    }
    
    public List<CovidCase> findByCountry(String country) {
    	LOG.debug("Debug Message in find by country");
    	LOG.fatal("Fatal Message in find by country");
    	
    	List<CovidCase> result = covidRepository.findByCountry(country);
    	LOG.info("query by country " + country + ": " + result.size());     	
    	return result;
    }
    
    public List<String> getAllCountries() {
    	LOG.debug("Debug Message in find All Countries");
    	LOG.fatal("Fatal Message in find All Countries");
    	
    	List<String> result = covidRepository.getAllCountries();
    	LOG.info("query All countries" + result.size());     	
    	return result;
    }
	
}
